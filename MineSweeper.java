import java.util.Scanner;

public class MineSweeper {

    public static void main(String[] args) {
        System.out.println("Welcome to MineSweeper!");
        boolean cont= true;

        while(cont) {
            cont = false;
            int difficulty = 0;
            int size = 0;
            Scanner sc=new Scanner(System.in);

            System.out.println("Please select a board size (Min: 10 / Max:50): ");
            boolean isInvalid;
            do {
                isInvalid = false;
                try {
                    int num = sc.nextInt ();
                    size = num;
                } catch (Exception e) {
                    isInvalid = true;
                    System.out.println("Invalid, please enter a number without decimals:");
                    sc.next ();
                } if (size < 10 || size > 50){
                    System.out.println("Invalid, please enter a size higher than 9");
                    isInvalid = true;
                }
            } while (isInvalid);

            System.out.println("please select a difficulty (1:Easy 2:Normal 3:Hard): ");
            do {
                isInvalid = false;
                try {
                    int num = sc.nextInt ();
                    difficulty = num;
                } catch (Exception e) {
                    isInvalid = true;
                    System.out.println("Invalid, please enter a number without decimals:");
                    sc.next ();
                } if (difficulty < 1 || difficulty > 3){
                    System.out.println("Invalid, please enter a valid difficulty:");
                    isInvalid = true;
                }
            } while (isInvalid);
            
            System.out.println("Here is your Board!");
            Board game = new Board(difficulty,size);
            System.out.print(game);

            System.out.println("Input a Row Number:");
            int row=0;
            int col=0;
            int option=0;
            do {
                row = 0;
                isInvalid = false;
                try {
                    int num = sc.nextInt ();
                    row = num;
                } catch (Exception e) {
                    isInvalid = true;
                    System.out.println("Invalid, please enter a number without decimals: ");
                    sc.next ();
                } if (row < 1 || row > size){
                    System.out.println("Invalid, please enter a row in the correct range: ");
                    isInvalid = true;
                }
            } while (isInvalid);

            System.out.println("Input a column Number:");
            do {
                col = 0;
                isInvalid = false;
                try {
                    int num = sc.nextInt ();
                    col = num;
                } catch (Exception e) {
                    isInvalid = true;
                    System.out.println("Invalid, please enter a number without decimals: ");
                    sc.next ();
                } if (col < 1 || col > size){
                    System.out.println("Invalid, please enter a row in the correct range: ");
                    isInvalid = true;
                }
            } while (isInvalid);
            
            game.firstClick(row-1, col-1);
            System.out.println("Board: ");
            System.out.print(game);

            while (game.getGameState() == 0){
                System.out.println("Input a Row Number:");
                do {
                    row = 0;
                    isInvalid = false;
                    try {
                        int num = sc.nextInt ();
                        row = num;
                    } catch (Exception e) {
                        isInvalid = true;
                        System.out.println("Invalid, please enter a number without decimals: ");
                        sc.next ();
                    } if (row < 1 || row > size){
                        System.out.println("Invalid, please enter a row in the correct range: ");
                        isInvalid = true;
                    }
                } while (isInvalid);

                System.out.println("Input a column Number:");
                do {
                    col = 0;
                    isInvalid = false;
                    try {
                        int num = sc.nextInt ();
                        col = num;
                    } catch (Exception e) {
                        isInvalid = true;
                        System.out.println("Invalid, please enter a number without decimals: ");
                        sc.next ();
                    } if (col < 1 || col > size){
                        System.out.println("Invalid, please enter a column in the correct range: ");
                        isInvalid = true;
                    }
                } while (isInvalid);

                System.out.println("Would you like to Flag or Click (1:Flag 2:Click)?");
                do {
                    option = 0;
                    isInvalid = false;
                    try {
                        int num = sc.nextInt();
                        option = num;
                    } catch (Exception e) {
                        isInvalid = true;
                        System.out.println("Invalid, please enter a valid number: ");
                        sc.next ();
                    } if (option < 1 || option > 2){
                        System.out.println("Invalid, please enter an option That is either (1) Flag or (2) Click: ");
                        isInvalid = true;
                    }
                } while (isInvalid);

                game.click(row-1, col-1, option);
                System.out.println("Board: ");
                System.out.print(game);
            }

            if (game.getGameState() == 1) {
                System.out.println("Oops that appears to be a Mine! Thanks for Playing!");
            } else if (game.getGameState() == 2){
                System.out.println("Wow great job, you cleared the entire board without hitting a mine!");
            }

            System.out.println("would you like to start a new game? Yes or No: ");
            String user = "";
            sc.nextLine();
            user = sc.nextLine().toLowerCase();

            if (user.equals("no")){
                cont = false;
                sc.close();
            } else if (user.equals("yes")){
                cont = true;
            }
        }
    }
}