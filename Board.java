import java.util.Random;

public class Board {
    private Cells[][][] mineBoard;
    private int difficulty;
    private int size;
    private int openTileCount;
    private int gameState;




    public Board(int difficulty, int size) {
        this.size = size;
        this.difficulty = difficulty;
        this.gameState = 0;
        
        mineBoard = new Cells[this.size][this.size][2];
        for (int i = 0; i<this.size; i++) {
            for(int j = 0; j<this.size; j++) {
                mineBoard[i][j][0] = Cells.UNCLICKED;
                mineBoard[i][j][1] = Cells.TBA;
            }
        }

        int bombCount = 0;
        if (this.difficulty == 1) {
            bombCount = Math.round(((this.size*this.size)/100)*10);

        } else if (this.difficulty == 2){
            bombCount = Math.round(((this.size*this.size)/100)*15);

        } else {
            bombCount = Math.round(((this.size*this.size)/100)*25);
        }

        this.openTileCount = (this.size*this.size)-bombCount;
        generateBombs(bombCount);
        setNumbers();
    }




    public boolean firstClick(int row, int column){
        if (row > this.size-1 || column > this.size-1 || row < 0 || column < 0) {
            return false;

        } else {
            if (mineBoard[row][column][1]==Cells.BOMB){
                this.openTileCount++;                
            }
            
            mineBoard[row][column][1] = Cells.EMPTY;

            setNumbers();

            mineBoard[row][column][0] = mineBoard[row][column][1];
            mineBoard[row][column][1] = Cells.EMPTY; 

            if (mineBoard[row][column][0]==Cells.EMPTY){
                reveal(row, column,0);
            }

            return true;
        }
    }




    public void click(int row, int column, int option){
        if(mineBoard[row][column][1]==Cells.TBA && option == 2){
            reveal(row,column,0);

        } else if (mineBoard[row][column][1]==Cells.EMPTY){
            System.out.println("This tile has already been revealed");

        } else if ((mineBoard[row][column][0]==Cells.UNCLICKED) && option == 1){
            flagging(row, column);

        } else if ((mineBoard[row][column][0]==Cells.FLAGGED) && option == 2){
            System.out.println("This tile is currently flagged. To remove the flag re-use the flag option on this tile.");

        } else if ((mineBoard[row][column][0]==Cells.FLAGGED) && option == 1){
            flagging(row, column);

        }else {
            reveal(row,column,1);
        }
    }




    private void flagging(int row, int column){
        if (mineBoard[row][column][0]==Cells.FLAGGED){
            mineBoard[row][column][0] = Cells.UNCLICKED;

        } else {
            mineBoard[row][column][0] = Cells.FLAGGED;
        }
    }



    public int getGameState(){
        return this.gameState;
    }




    public String toString() {
        int counter = 1;
        String rowColumn = "   ";

        for(int k = 1; k < this.size + 1; k++) {
            if (k < this.size && k < 10) {
                rowColumn += k + "  ";

            } else if (k < this.size && k > 9) {
                rowColumn += k + " ";

            } else if (k == this.size) {
                rowColumn += k + "\n";
            }
        }

		for(int i = 0; i < mineBoard.length;i++){
            if (i < 9){
                rowColumn += counter + "  ";

            } else {
                rowColumn += counter + " ";

            }
			for(int j = 0; j < mineBoard[i].length;j++){
                rowColumn += mineBoard[i][j][0] + "  ";
			}

			rowColumn+="\n";
			counter++;
		}

		return rowColumn;
    }




    private void generateBombs(int count) {
        Random rand = new Random();
        int vert = 0;
        int hori = 0;

        //Randomly Generates Mines
        for (int i = 0; i <= count; i++){
            vert = rand.nextInt(this.size);
            hori = rand.nextInt(this.size);
            mineBoard[vert][hori][1] = Cells.BOMB;            
        }
    }




    private void setNumbers() {
        for(int i = 0; i < mineBoard.length; i++) {
            int bombCounter = 0;
            for (int k = 0; k < mineBoard[i].length; k++){
                if(mineBoard[i][k][1]!=Cells.BOMB){
                    //Checks Top-Left all the way to 1 Tile before Top-Right
                    if (i == 0 && k == 0 || i == 0 && k < this.size-1) {
                        if (mineBoard[i][k+1][1]==Cells.BOMB){
                            bombCounter++;

                        } if (mineBoard[i+1][k][1]==Cells.BOMB){
                            bombCounter++;

                        } if (mineBoard[i+1][k+1][1]==Cells.BOMB){
                            bombCounter++;
                        }
                        if (i == 0 && k < this.size-1 && k > 0){
                            if (mineBoard[i][k-1][1]==Cells.BOMB){
                                bombCounter++;

                            } if (mineBoard[i+1][k-1][1]==Cells.BOMB){
                                bombCounter++;
                            }
                        }      
                    }
                    //Checks Top-Right all the way to 1 Tile before Botom-Right
                    if (i >= 0 && k == this.size-1 && i < this.size-1){
                        if (mineBoard[i][k-1][1]==Cells.BOMB) {
                            bombCounter++;

                        } if (mineBoard[i+1][k-1][1]==Cells.BOMB){
                            bombCounter++;

                        } if (mineBoard[i+1][k][1]==Cells.BOMB){
                            bombCounter++;
                        }
                        if (i > 0 && k == this.size-1 && i < this.size-1){
                            if (mineBoard[i-1][k-1][1]==Cells.BOMB){
                                bombCounter++;

                            } if (mineBoard[i-1][k][1]==Cells.BOMB){
                                bombCounter++;
                            }
                        }
                    }                  
                    //Checks Bottom-Right all the way to 1 Tile before Bottom-Left
                    if (i == this.size-1 && k > 0){
                        if (mineBoard[i-1][k-1][1]==Cells.BOMB) {
                            bombCounter++;

                        } if (mineBoard[i-1][k][1]==Cells.BOMB){
                            bombCounter++;

                        } if (mineBoard[i][k-1][1]==Cells.BOMB){
                            bombCounter++;
                        }
                        if (i == this.size-1 && k > 0 && k < this.size-1){
                            if (mineBoard[i-1][k+1][1]==Cells.BOMB){
                                bombCounter++;

                            } if (mineBoard[i][k+1][1]==Cells.BOMB){
                                bombCounter++;
                            }
                        }
                    }
                    //Checks Bottom-Left all the way to 1 Tile before Top-Left   3,0
                    if (k == 0 && i == this.size-1 || k == 0 && i < this.size-1 && i > 0){
                        if (mineBoard[i-1][k][1]==Cells.BOMB) {
                            bombCounter++;

                        } if (mineBoard[i-1][k+1][1]==Cells.BOMB){
                            bombCounter++;

                        } if (mineBoard[i][k+1][1]==Cells.BOMB){
                            bombCounter++;
                        }
                        if (k == 0 && i < this.size-1 && i > 0){
                            if (mineBoard[i+1][k][1]==Cells.BOMB){
                                bombCounter++;

                            } if (mineBoard[i+1][k+1][1]==Cells.BOMB){
                                bombCounter++;
                            }
                        }
                    }
                    //Checks everywhere that is 1 Tile away from the sides
                    if (k > 0 && k < this.size-1 && i > 0 && i < this.size-1) {
                        for (int l=(-1); l<2; l++){
                            for (int q=(-1); q<2; q++){
                                if (mineBoard[i+l][k+q][1]==Cells.BOMB) {
                                    bombCounter++;
                                }
                            }
                        }
                    }
                    //Sets the number for that coresponding Cell
                    if (bombCounter == 1) {
                        mineBoard[i][k][1] = Cells.ONE;

                    } else if (bombCounter == 2) {
                        mineBoard[i][k][1] = Cells.TWO;

                    } else if (bombCounter == 3){
                        mineBoard[i][k][1] = Cells.THREE;

                    } else if (bombCounter == 4){
                        mineBoard[i][k][1] = Cells.FOUR;

                    } else if (bombCounter == 5){
                        mineBoard[i][k][1] = Cells.FIVE;

                    } else if (bombCounter == 6){
                        mineBoard[i][k][1] = Cells.SIX;

                    } else if (bombCounter == 7){
                        mineBoard[i][k][1] = Cells.SEVEN;

                    } else if (bombCounter == 8){
                        mineBoard[i][k][1] = Cells.EIGHT;

                    }
                    bombCounter=0;
                }
            }
        }
    }




    private void reveal(int row, int column, int type){
        int rowTemp=0;
        int ColTemp=0;

        if(type == 0){
            for (int i=(-1); i<2; i++){
                for (int k=(-1); k<2; k++){
                    if ((row+i < this.size &&  row+i >= 0) && (column+k < this.size && column+k >= 0) && (mineBoard[row+i][column+k][0]==Cells.UNCLICKED)){
                        if (mineBoard[row+i][column+k][1]==Cells.TBA){
                            mineBoard[row+i][column+k][0] = Cells.EMPTY;
                            mineBoard[row+i][column+k][1] = Cells.EMPTY;
                            rowTemp=row+i;
                            ColTemp=column+k;
                            reveal(rowTemp,ColTemp,0);

                        } if (mineBoard[row+i][column+k][1]==Cells.ONE || mineBoard[row+i][column+k][1]==Cells.TWO || mineBoard[row+i][column+k][1]==Cells.THREE ||
                                mineBoard[row+i][column+k][1]==Cells.FOUR || mineBoard[row+i][column+k][1]==Cells.FIVE || mineBoard[row+i][column+k][1]==Cells.SIX || 
                                mineBoard[row+i][column+k][1]==Cells.SEVEN || mineBoard[row+i][column+k][1]==Cells.EIGHT)
                                {
                                    setEmptyNumber(row+i,column+k);
                            }
                        } else {
                            continue;
                    }
                }
            }
        } else {
            if (mineBoard[row][column][1]==Cells.TBA){

                mineBoard[row][column][0] = Cells.EMPTY;
                mineBoard[row][column][1] = Cells.EMPTY;
            }
            if (mineBoard[row][column][1]==Cells.BOMB || mineBoard[row][column][1]==Cells.ONE || mineBoard[row][column][1]==Cells.TWO ||
                mineBoard[row][column][1]==Cells.THREE || mineBoard[row][column][1]==Cells.FOUR || mineBoard[row][column][1]==Cells.FIVE ||
                mineBoard[row][column][1]==Cells.SIX || mineBoard[row][column][1]==Cells.SEVEN || mineBoard[row][column][1]==Cells.EIGHT) 
                {
                setEmptyNumber(row,column);
            }
        }
        checkGameState();
    }




    private void setEmptyNumber(int row, int column){
        mineBoard[row][column][0] = mineBoard[row][column][1];
        mineBoard[row][column][1] = Cells.EMPTY;
    }




    private void checkGameState(){
        int clearedTileCounter=0;
        for (int i = 0; i<this.size; i++) {
            for(int j = 0; j<this.size; j++) {
                if (mineBoard[i][j][0]==Cells.BOMB){
                    this.gameState = 1;
                    return;

                } else if (mineBoard[i][j][1]==Cells.EMPTY) {
                    clearedTileCounter++;                   
                    if (clearedTileCounter >= this.openTileCount){
                        this.gameState = 2;
                        return;
                    }
                }
            }
        }
    }  
}



 
