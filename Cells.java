public enum Cells {
    EMPTY, BOMB, FLAGGED, UNCLICKED, TBA,
    ONE, TWO, THREE, FOUR, FIVE, SIX, SEVEN, EIGHT;

    public String toString() {
        String empty = "\u00A0";
        String bomb = "\u26DD";
        String flag = "\u26BF";
        String wait = "\u26F6";
        String one = "\u00B9";
        String two = "\u00B2";
        String three = "\u00B3";
        String four = "\u2074";
        String five = "\u2075";
        String six = "\u2076";
        String seven = "\u2075";
        String eight = "\u2078";

        if(this == EMPTY || this == TBA){
            return empty;
        } else if(this == BOMB) {
            return bomb;
        } else if (this == FLAGGED) {
            return flag;
        } else if(this == UNCLICKED) {
            return wait;
        } else if (this == ONE) {
            return one;
        } else if (this == TWO) {
            return two;
        } else if (this == THREE) {
            return three;
        } else if (this == FOUR) {
            return four;
        } else if (this == FIVE) {
            return five;
        } else if (this == SIX) {
            return six;
        } else if (this == SEVEN) {
            return seven;
        } else {
            return eight;
        }
    }
}
